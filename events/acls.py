from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # create some query parameters
    params = {
        "query": f'{city}, {state}',
        "per_page": "1",
    }
    # construct the url
    url = 'https://api.pexels.com/v1/search'
    # create some headers
    headers = {'Authorization': PEXELS_API_KEY}
    # send the request
    response = requests.get(url, headers=headers, params=params)
    try:
        # parse the json
        content = response.json()
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except(KeyError, IndexError, requests.exceptions.JSONDecoderError):
        return {"picture_url": None}

def get_weather_data(city, state):
    # geocoding
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f'{city},{state},US',
        "appid": "49dfe50756c388896eda49173c9009be",
        "limit": "1",
    }
    response = requests.get(geo_url, params=params)
    content = response.json()
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    # weather request
    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    params = {
        "appid": "49dfe50756c388896eda49173c9009be",
        "lat": lat,
        "lon": lon,
    }
    response = requests.get(weather_url, params=params)
    content = response.json()
    print(content)
    return {
        "temp": content["weather"][0]["main"],
        "description": content["weather"][0]["description"],
    }
